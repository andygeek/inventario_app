﻿using Entity;
namespace Data
{
    public interface IBrandRepository : ICrudRepository<Brand>
    {

    }
}
