-- Created by Vertabelo (http://vertabelo.com)
-- Last modification date: 2019-04-22 01:34:53.784

-- tables
-- Table: brand
CREATE TABLE brand (
    id int  NOT NULL,
    name varchar(100)  NOT NULL,
    CONSTRAINT brand_pk PRIMARY KEY  (id)
);

-- Table: category
CREATE TABLE category (
    id int  NOT NULL,
    name varchar(150)  NOT NULL,
    CONSTRAINT category_pk PRIMARY KEY  (id)
);

-- Table: client
CREATE TABLE client (
    id int  NOT NULL,
    name varchar(150)  NOT NULL,
    CONSTRAINT client_pk PRIMARY KEY  (id)
);

-- Table: model
CREATE TABLE model (
    id int  NOT NULL,
    brands_id int  NOT NULL,
    name varchar(100)  NOT NULL,
    CONSTRAINT model_pk PRIMARY KEY  (id,brands_id)
);

-- Table: product
CREATE TABLE product (
    id int  NOT NULL,
    category_id int  NOT NULL,
    subcategory_id int  NOT NULL,
    description varchar(150)  NOT NULL,
    stock int  NOT NULL,
    model_id int  NULL,
    model_brands_id int  NULL,
    price decimal(7,2)  NOT NULL,
    cost decimal(7,2)  NOT NULL,
    CONSTRAINT product_pk PRIMARY KEY  (id)
);

-- Table: product_entry
CREATE TABLE product_entry (
    id int  NOT NULL,
    providers_id int  NOT NULL,
    total decimal(9,2)  NOT NULL,
    datatime datetime  NOT NULL,
    CONSTRAINT product_entry_pk PRIMARY KEY  (id)
);

-- Table: product_entry_detail
CREATE TABLE product_entry_detail (
    entry_id int  NOT NULL,
    product_id int  NOT NULL,
    quantity decimal(10,2)  NOT NULL,
    cost decimal(9,2)  NOT NULL,
    subtotal decimal(9,2)  NOT NULL,
    CONSTRAINT product_entry_detail_pk PRIMARY KEY  (entry_id,product_id)
);

-- Table: provider
CREATE TABLE provider (
    id int  NOT NULL,
    name varchar(150)  NOT NULL,
    address varchar(250)  NULL,
    phone varchar(10)  NULL,
    email varchar(100)  NULL,
    CONSTRAINT provider_pk PRIMARY KEY  (id)
);

-- Table: remove_product
CREATE TABLE remove_product (
    id int  NOT NULL,
    datatime datetime  NOT NULL,
    reason varchar(250)  NOT NULL,
    CONSTRAINT remove_product_pk PRIMARY KEY  (id)
);

-- Table: remove_product_detail
CREATE TABLE remove_product_detail (
    products_id int  NOT NULL,
    remove_products_id int  NOT NULL,
    quantity int  NOT NULL,
    price decimal(7,2)  NOT NULL,
    cost decimal(7,2)  NOT NULL,
    CONSTRAINT remove_product_detail_pk PRIMARY KEY  (products_id,remove_products_id)
);

-- Table: sales
CREATE TABLE sales (
    id int  NOT NULL,
    client_id int  NOT NULL,
    remove_product_id int  NOT NULL,
    subtotal decimal(7,2)  NOT NULL,
    discount decimal(7,2)  NOT NULL,
    total decimal(7,2)  NOT NULL,
    CONSTRAINT sales_pk PRIMARY KEY  (id)
);

-- Table: subcategory
CREATE TABLE subcategory (
    id int  NOT NULL,
    category_id int  NOT NULL,
    name varchar(100)  NOT NULL,
    CONSTRAINT subcategory_pk PRIMARY KEY  (id,category_id)
);

-- foreign keys
-- Reference: detail_remove_product_products (table: remove_product_detail)
ALTER TABLE remove_product_detail ADD CONSTRAINT detail_remove_product_products
    FOREIGN KEY (products_id)
    REFERENCES product (id);

-- Reference: detail_remove_product_remove_products (table: remove_product_detail)
ALTER TABLE remove_product_detail ADD CONSTRAINT detail_remove_product_remove_products
    FOREIGN KEY (remove_products_id)
    REFERENCES remove_product (id);

-- Reference: entry_detail_product_entry (table: product_entry_detail)
ALTER TABLE product_entry_detail ADD CONSTRAINT entry_detail_product_entry
    FOREIGN KEY (entry_id)
    REFERENCES product_entry (id);

-- Reference: entry_detail_products (table: product_entry_detail)
ALTER TABLE product_entry_detail ADD CONSTRAINT entry_detail_products
    FOREIGN KEY (product_id)
    REFERENCES product (id);

-- Reference: model_brands (table: model)
ALTER TABLE model ADD CONSTRAINT model_brands
    FOREIGN KEY (brands_id)
    REFERENCES brand (id);

-- Reference: product_entry_providers (table: product_entry)
ALTER TABLE product_entry ADD CONSTRAINT product_entry_providers
    FOREIGN KEY (providers_id)
    REFERENCES provider (id);

-- Reference: products_model (table: product)
ALTER TABLE product ADD CONSTRAINT products_model
    FOREIGN KEY (model_id,model_brands_id)
    REFERENCES model (id,brands_id);

-- Reference: products_subcategories (table: product)
ALTER TABLE product ADD CONSTRAINT products_subcategories
    FOREIGN KEY (subcategory_id,category_id)
    REFERENCES subcategory (id,category_id);

-- Reference: sales_client (table: sales)
ALTER TABLE sales ADD CONSTRAINT sales_client
    FOREIGN KEY (client_id)
    REFERENCES client (id);

-- Reference: sales_remove_product (table: sales)
ALTER TABLE sales ADD CONSTRAINT sales_remove_product
    FOREIGN KEY (remove_product_id)
    REFERENCES remove_product (id);

-- Reference: subcategories_categories (table: subcategory)
ALTER TABLE subcategory ADD CONSTRAINT subcategories_categories
    FOREIGN KEY (category_id)
    REFERENCES category (id);

-- End of file.

