﻿namespace Entity
{
    public class Provider
    {
        public int id { get; set; }
        public string name { get; set; }
        public string adress { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
    }
}
