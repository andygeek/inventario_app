﻿namespace Entity
{
    public class Product
    {
        public int id { get; set; }
        public Category category { get; set; }
        public Subcategory subcategory { get; set; }
        public string description { get; set; }
        public int stock { get; set; }
        public Model model { get; set; }
        public Brand brand { get; set; }
        public decimal price { get; set; }
    }
}
