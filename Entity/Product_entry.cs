﻿using System;

namespace Entity
{
    public class Product_entry
    {
        public int id { get; set; }
        public decimal total { get; set; }
        public DateTime datatime { get; set; }
        public Provider provider { get; set; }
    }
}
