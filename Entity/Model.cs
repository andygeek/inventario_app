﻿namespace Entity
{
    public class Model
    {
        public int id { get; set; }
        public string name { get; set; }
        public Brand brand { get; set; }
    }
}
