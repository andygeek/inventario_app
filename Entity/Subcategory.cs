﻿namespace Entity
{
    public class Subcategory
    {
        public int id { get; set; }
        public string name { get; set; }
        public Category category { get; set; }
    }
}
